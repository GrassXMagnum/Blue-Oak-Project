﻿namespace ProtoypeBlueOakINC
{
    partial class EditWorkOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditWorkOrderForm));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.txtACA = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtNoteRepairs = new System.Windows.Forms.TextBox();
            this.chkRepairs = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNoteChecks = new System.Windows.Forms.TextBox();
            this.chkChecks = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNotePaperwork = new System.Windows.Forms.TextBox();
            this.chkPaperwork = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNoteLogbook = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNoteManuals = new System.Windows.Forms.TextBox();
            this.chkLogbook = new System.Windows.Forms.CheckBox();
            this.cbxManuals = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.finalized = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.discrepanciesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workCardColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditWorkCardButtonColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.IDWorkCard = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRegOrPN = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSerial = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPropellerTotalTime = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTotalCycles = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEngineTotalTime = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTotalTime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIDWorkOrder = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workOrderxmlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workOrderOnlyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wOAndWorkCardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hTMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            this.toolStripContainer1.BottomToolStripPanelVisible = false;
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.panel1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(528, 389);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(528, 414);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoScrollMargin = new System.Drawing.Size(0, 100);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txtACA);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txtNoteRepairs);
            this.panel1.Controls.Add(this.chkRepairs);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtNoteChecks);
            this.panel1.Controls.Add(this.chkChecks);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtNotePaperwork);
            this.panel1.Controls.Add(this.chkPaperwork);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textBox10);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtNoteLogbook);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtNoteManuals);
            this.panel1.Controls.Add(this.chkLogbook);
            this.panel1.Controls.Add(this.cbxManuals);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtRegOrPN);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtSerial);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtPropellerTotalTime);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtTotalCycles);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtEngineTotalTime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtTotalTime);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtIDWorkOrder);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(528, 389);
            this.panel1.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(189, 598);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 59;
            this.label16.Text = "ACA # :";
            // 
            // txtACA
            // 
            this.txtACA.Location = new System.Drawing.Point(239, 595);
            this.txtACA.Name = "txtACA";
            this.txtACA.Size = new System.Drawing.Size(57, 20);
            this.txtACA.TabIndex = 58;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(197, 562);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 57;
            this.label15.Text = "Note :";
            // 
            // txtNoteRepairs
            // 
            this.txtNoteRepairs.Location = new System.Drawing.Point(239, 549);
            this.txtNoteRepairs.Multiline = true;
            this.txtNoteRepairs.Name = "txtNoteRepairs";
            this.txtNoteRepairs.Size = new System.Drawing.Size(241, 40);
            this.txtNoteRepairs.TabIndex = 56;
            // 
            // chkRepairs
            // 
            this.chkRepairs.AutoSize = true;
            this.chkRepairs.Location = new System.Drawing.Point(50, 549);
            this.chkRepairs.Name = "chkRepairs";
            this.chkRepairs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkRepairs.Size = new System.Drawing.Size(132, 43);
            this.chkRepairs.TabIndex = 55;
            this.chkRepairs.Text = "All Required Major\r\nRepair or Modification \r\nReport Completed";
            this.chkRepairs.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkRepairs.ThreeState = true;
            this.chkRepairs.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(197, 514);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 13);
            this.label14.TabIndex = 54;
            this.label14.Text = "Note :";
            // 
            // txtNoteChecks
            // 
            this.txtNoteChecks.Location = new System.Drawing.Point(239, 503);
            this.txtNoteChecks.Multiline = true;
            this.txtNoteChecks.Name = "txtNoteChecks";
            this.txtNoteChecks.Size = new System.Drawing.Size(241, 40);
            this.txtNoteChecks.TabIndex = 53;
            // 
            // chkChecks
            // 
            this.chkChecks.AutoSize = true;
            this.chkChecks.Location = new System.Drawing.Point(33, 503);
            this.chkChecks.Name = "chkChecks";
            this.chkChecks.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkChecks.Size = new System.Drawing.Size(149, 30);
            this.chkChecks.TabIndex = 52;
            this.chkChecks.Text = "All Required Independent \r\nChecks Completed";
            this.chkChecks.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkChecks.ThreeState = true;
            this.chkChecks.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(197, 466);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 51;
            this.label13.Text = "Note :";
            // 
            // txtNotePaperwork
            // 
            this.txtNotePaperwork.Location = new System.Drawing.Point(239, 457);
            this.txtNotePaperwork.Multiline = true;
            this.txtNotePaperwork.Name = "txtNotePaperwork";
            this.txtNotePaperwork.Size = new System.Drawing.Size(241, 40);
            this.txtNotePaperwork.TabIndex = 50;
            // 
            // chkPaperwork
            // 
            this.chkPaperwork.AutoSize = true;
            this.chkPaperwork.Location = new System.Drawing.Point(52, 457);
            this.chkPaperwork.Name = "chkPaperwork";
            this.chkPaperwork.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkPaperwork.Size = new System.Drawing.Size(130, 30);
            this.chkPaperwork.TabIndex = 49;
            this.chkPaperwork.Text = "All Required \r\nPaperwork Completed";
            this.chkPaperwork.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkPaperwork.ThreeState = true;
            this.chkPaperwork.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(30, 366);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 13);
            this.label11.TabIndex = 46;
            this.label11.Text = "Customer Approval";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(33, 382);
            this.textBox10.Multiline = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(447, 64);
            this.textBox10.TabIndex = 45;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(197, 341);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 44;
            this.label10.Text = "Note :";
            // 
            // txtNoteLogbook
            // 
            this.txtNoteLogbook.Location = new System.Drawing.Point(239, 338);
            this.txtNoteLogbook.Name = "txtNoteLogbook";
            this.txtNoteLogbook.Size = new System.Drawing.Size(241, 20);
            this.txtNoteLogbook.TabIndex = 43;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(197, 316);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 42;
            this.label9.Text = "Note :";
            // 
            // txtNoteManuals
            // 
            this.txtNoteManuals.Location = new System.Drawing.Point(239, 313);
            this.txtNoteManuals.Name = "txtNoteManuals";
            this.txtNoteManuals.Size = new System.Drawing.Size(241, 20);
            this.txtNoteManuals.TabIndex = 41;
            // 
            // chkLogbook
            // 
            this.chkLogbook.AutoSize = true;
            this.chkLogbook.Location = new System.Drawing.Point(29, 340);
            this.chkLogbook.Name = "chkLogbook";
            this.chkLogbook.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkLogbook.Size = new System.Drawing.Size(142, 17);
            this.chkLogbook.TabIndex = 40;
            this.chkLogbook.Text = "Log Book Discrepancies";
            this.chkLogbook.UseVisualStyleBackColor = true;
            // 
            // cbxManuals
            // 
            this.cbxManuals.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbxManuals.FormattingEnabled = true;
            this.cbxManuals.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxManuals.Items.AddRange(new object[] {
            "Customer",
            "AMO",
            "Other"});
            this.cbxManuals.Location = new System.Drawing.Point(90, 313);
            this.cbxManuals.Name = "cbxManuals";
            this.cbxManuals.Size = new System.Drawing.Size(81, 21);
            this.cbxManuals.TabIndex = 39;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 316);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Manuals :";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToResizeColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.finalized,
            this.discrepanciesColumn,
            this.workCardColumn,
            this.EditWorkCardButtonColumn,
            this.IDWorkCard});
            this.dataGridView1.Location = new System.Drawing.Point(33, 139);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(447, 168);
            this.dataGridView1.TabIndex = 37;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // finalized
            // 
            this.finalized.FalseValue = "";
            this.finalized.HeaderText = "Finalized";
            this.finalized.Name = "finalized";
            this.finalized.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.finalized.Visible = false;
            // 
            // discrepanciesColumn
            // 
            this.discrepanciesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.discrepanciesColumn.FillWeight = 200F;
            this.discrepanciesColumn.HeaderText = "Discrepancies";
            this.discrepanciesColumn.Name = "discrepanciesColumn";
            // 
            // workCardColumn
            // 
            this.workCardColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.workCardColumn.HeaderText = "Work Card #";
            this.workCardColumn.Name = "workCardColumn";
            // 
            // EditWorkCardButtonColumn
            // 
            this.EditWorkCardButtonColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EditWorkCardButtonColumn.FillWeight = 50F;
            this.EditWorkCardButtonColumn.HeaderText = "Edit...";
            this.EditWorkCardButtonColumn.Name = "EditWorkCardButtonColumn";
            this.EditWorkCardButtonColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // IDWorkCard
            // 
            this.IDWorkCard.HeaderText = "IDWorkCard";
            this.IDWorkCard.Name = "IDWorkCard";
            this.IDWorkCard.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Registration or P/N :";
            // 
            // txtRegOrPN
            // 
            this.txtRegOrPN.Location = new System.Drawing.Point(140, 61);
            this.txtRegOrPN.Name = "txtRegOrPN";
            this.txtRegOrPN.Size = new System.Drawing.Size(152, 20);
            this.txtRegOrPN.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(340, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "S/N :";
            // 
            // txtSerial
            // 
            this.txtSerial.Location = new System.Drawing.Point(380, 61);
            this.txtSerial.Name = "txtSerial";
            this.txtSerial.Size = new System.Drawing.Size(100, 20);
            this.txtSerial.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(299, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Propeller T.T :";
            // 
            // txtPropellerTotalTime
            // 
            this.txtPropellerTotalTime.Location = new System.Drawing.Point(380, 113);
            this.txtPropellerTotalTime.Name = "txtPropellerTotalTime";
            this.txtPropellerTotalTime.Size = new System.Drawing.Size(100, 20);
            this.txtPropellerTotalTime.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(302, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Total Cycles :";
            // 
            // txtTotalCycles
            // 
            this.txtTotalCycles.Location = new System.Drawing.Point(380, 87);
            this.txtTotalCycles.Name = "txtTotalCycles";
            this.txtTotalCycles.Size = new System.Drawing.Size(100, 20);
            this.txtTotalCycles.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Engine T.T :";
            // 
            // txtEngineTotalTime
            // 
            this.txtEngineTotalTime.Location = new System.Drawing.Point(140, 113);
            this.txtEngineTotalTime.Name = "txtEngineTotalTime";
            this.txtEngineTotalTime.Size = new System.Drawing.Size(100, 20);
            this.txtEngineTotalTime.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Total time :";
            // 
            // txtTotalTime
            // 
            this.txtTotalTime.Location = new System.Drawing.Point(140, 87);
            this.txtTotalTime.Name = "txtTotalTime";
            this.txtTotalTime.Size = new System.Drawing.Size(100, 20);
            this.txtTotalTime.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Work Order # :";
            // 
            // txtIDWorkOrder
            // 
            this.txtIDWorkOrder.Location = new System.Drawing.Point(140, 20);
            this.txtIDWorkOrder.Name = "txtIDWorkOrder";
            this.txtIDWorkOrder.Size = new System.Drawing.Size(152, 20);
            this.txtIDWorkOrder.TabIndex = 23;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2,
            this.toolStripDropDownButton3});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(166, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.printToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(38, 22);
            this.toolStripDropDownButton1.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem.Text = "New...";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(132, 22);
            this.toolStripMenuItem1.Text = "WorkOrder";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.workOrderxmlToolStripMenuItem});
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "Open...";
            // 
            // workOrderxmlToolStripMenuItem
            // 
            this.workOrderxmlToolStripMenuItem.Name = "workOrderxmlToolStripMenuItem";
            this.workOrderxmlToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.workOrderxmlToolStripMenuItem.Text = "Work Order (xml)";
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.workOrderOnlyToolStripMenuItem,
            this.wOAndWorkCardsToolStripMenuItem,
            this.allToolStripMenuItem});
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printToolStripMenuItem.Text = "Print...";
            // 
            // workOrderOnlyToolStripMenuItem
            // 
            this.workOrderOnlyToolStripMenuItem.Name = "workOrderOnlyToolStripMenuItem";
            this.workOrderOnlyToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.workOrderOnlyToolStripMenuItem.Text = "Work Order only";
            // 
            // wOAndWorkCardsToolStripMenuItem
            // 
            this.wOAndWorkCardsToolStripMenuItem.Name = "wOAndWorkCardsToolStripMenuItem";
            this.wOAndWorkCardsToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.wOAndWorkCardsToolStripMenuItem.Text = "Work Cards only";
            // 
            // allToolStripMenuItem
            // 
            this.allToolStripMenuItem.Name = "allToolStripMenuItem";
            this.allToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.allToolStripMenuItem.Text = "All";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pDFToolStripMenuItem,
            this.hTMLToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // pDFToolStripMenuItem
            // 
            this.pDFToolStripMenuItem.Name = "pDFToolStripMenuItem";
            this.pDFToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pDFToolStripMenuItem.Text = "PDF";
            this.pDFToolStripMenuItem.Click += new System.EventHandler(this.pDFToolStripMenuItem_Click);
            // 
            // hTMLToolStripMenuItem
            // 
            this.hTMLToolStripMenuItem.Name = "hTMLToolStripMenuItem";
            this.hTMLToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.hTMLToolStripMenuItem.Text = "HTML";
            this.hTMLToolStripMenuItem.Click += new System.EventHandler(this.hTMLToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(40, 22);
            this.toolStripDropDownButton2.Text = "Edit";
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(45, 22);
            this.toolStripDropDownButton3.Text = "Help";
            // 
            // EditWorkOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 414);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "EditWorkOrderForm";
            this.Text = "Blue Oak INC";
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workOrderxmlToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtNoteLogbook;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNoteManuals;
        private System.Windows.Forms.CheckBox chkLogbook;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRegOrPN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSerial;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPropellerTotalTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTotalCycles;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEngineTotalTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTotalTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIDWorkOrder;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNotePaperwork;
        private System.Windows.Forms.CheckBox chkPaperwork;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox txtNoteChecks;
        private System.Windows.Forms.CheckBox chkChecks;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workOrderOnlyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wOAndWorkCardsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allToolStripMenuItem;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtACA;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtNoteRepairs;
        private System.Windows.Forms.CheckBox chkRepairs;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbxManuals;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn finalized;
        private System.Windows.Forms.DataGridViewTextBoxColumn discrepanciesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn workCardColumn;
        private System.Windows.Forms.DataGridViewButtonColumn EditWorkCardButtonColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDWorkCard;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem hTMLToolStripMenuItem;
    }
}

