﻿using ProtoypeBlueOakINC.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProtoypeBlueOakINC
{
    public partial class EditWorkOrderForm : Form
    {

        public EditWorkOrderForm()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                //TODO - Open the Work Card Edit Form
            }
        }

        private void hTMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var currentDir = Directory.GetCurrentDirectory();
            var templateFilename = "templateWorkOrder.html";
            var htmlTemplateFilepath = Path.Combine(currentDir, templateFilename);
            var content = "";
            content = File.ReadAllText(htmlTemplateFilepath);

            content = content.Replace("{ID_WO}", txtIDWorkOrder.Text)
                .Replace("{REG_OR_PN}", txtRegOrPN.Text)
                .Replace("{SERIAL_NUM}", txtSerial.Text)
                .Replace("{TITLE}", "WorkOrder #" + txtIDWorkOrder.Text)
                .Replace("{TOTAL_TIME}", txtTotalTime.Text)
                .Replace("{TOTAL_CYCLES}", txtTotalCycles.Text)
                .Replace("{ENGINE_TT}", txtEngineTotalTime.Text)
                .Replace("{PROPELLER_TT}", txtPropellerTotalTime.Text)
                .Replace("{CHK_CUSTOMER}", cbxManuals.SelectedIndex == 0 ? "checked" : "")
                .Replace("{CHK_AMO}", cbxManuals.SelectedIndex == 1 ? "checked" : "")
                .Replace("{CHK_OTHER}", cbxManuals.SelectedIndex == 2 ? "checked" : "")
                .Replace("{NOTE_1}", txtNoteManuals.Text)
                .Replace("{CHK_LOGBOOK_YES}", chkLogbook.Checked ? "checked" : "")
                .Replace("{CHK_LOGBOOK_NO}", chkLogbook.Checked ? "" : "checked")
                .Replace("{NOTE_2}", txtNoteLogbook.Text)
                .Replace("{CHK_PAPERWORK_YES}", chkPaperwork.Checked ? "checked" : "")
                .Replace("{CHK_PAPERWORK_NO}", chkPaperwork.Checked ? "" : "checked")
                .Replace("{CHK_PAPERWORK_NA}", "")
                .Replace("{NOTE_3}", txtNotePaperwork.Text)
                .Replace("{CHK_INDCHECKS_YES}", chkChecks.Checked ? "checked" : "")
                .Replace("{CHK_INDCHECKS_NO}", chkChecks.Checked ? "" : "checked")
                .Replace("{CHK_INDCHECKS_NA}", "")
                .Replace("{NOTE_4}", txtNoteChecks.Text)
                .Replace("{CHK_REPAIRS_YES}", chkRepairs.Checked ? "checked" : "")
                .Replace("{CHK_REPAIRS_NO}", chkRepairs.Checked ? "" : "checked")
                .Replace("{CHK_REPAIRS_NA}", "")
                .Replace("{NOTE_5}", txtNoteRepairs.Text)
                .Replace("{ACA}", txtACA.Text);
            var strWorkCards = "";
            foreach (var row in dataGridView1.Rows) {
                var blah=(DataGridViewRow)row;
                strWorkCards += "<tr><td colspan=\"5\">" + blah.Cells[1].Value + " </td> <td colspan=\"3\">" + blah.Cells[2].Value + "</td></tr>";
            }
            content = content.Replace("{WORK_CARDS}", strWorkCards);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "HTML File|*.html";
            saveFileDialog1.Title = "Save an HTML File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (saveFileDialog1.FileName != "")
            {
                if (File.Exists(saveFileDialog1.FileName)) {
                    File.Delete(saveFileDialog1.FileName);
                }
                File.WriteAllText(saveFileDialog1.FileName, content);
            }  
        }

        private void pDFToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var currentDir = Directory.GetCurrentDirectory();
            var templateFilename = "templateWorkOrder.html";
            var htmlTemplateFilepath = Path.Combine(currentDir, templateFilename);
            var content = "";
            content = File.ReadAllText(htmlTemplateFilepath);

            content = content.Replace("{ID_WO}", txtIDWorkOrder.Text)
                .Replace("{REG_OR_PN}", txtRegOrPN.Text)
                .Replace("{SERIAL_NUM}", txtSerial.Text)
                .Replace("{TITLE}", "WorkOrder #" + txtIDWorkOrder.Text)
                .Replace("{TOTAL_TIME}", txtTotalTime.Text)
                .Replace("{TOTAL_CYCLES}", txtTotalCycles.Text)
                .Replace("{ENGINE_TT}", txtEngineTotalTime.Text)
                .Replace("{PROPELLER_TT}", txtPropellerTotalTime.Text)
                .Replace("{CHK_CUSTOMER}", cbxManuals.SelectedIndex == 0 ? "checked" : "")
                .Replace("{CHK_AMO}", cbxManuals.SelectedIndex == 1 ? "checked" : "")
                .Replace("{CHK_OTHER}", cbxManuals.SelectedIndex == 2 ? "checked" : "")
                .Replace("{NOTE_1}", txtNoteManuals.Text)
                .Replace("{CHK_LOGBOOK_YES}", chkLogbook.Checked ? "checked" : "")
                .Replace("{CHK_LOGBOOK_NO}", chkLogbook.Checked ? "" : "checked")
                .Replace("{NOTE_2}", txtNoteLogbook.Text)
                .Replace("{CHK_PAPERWORK_YES}", chkPaperwork.Checked ? "checked" : "")
                .Replace("{CHK_PAPERWORK_NO}", chkPaperwork.Checked ? "" : "checked")
                .Replace("{CHK_PAPERWORK_NA}", "")
                .Replace("{NOTE_3}", txtNotePaperwork.Text)
                .Replace("{CHK_INDCHECKS_YES}", chkChecks.Checked ? "checked" : "")
                .Replace("{CHK_INDCHECKS_NO}", chkChecks.Checked ? "" : "checked")
                .Replace("{CHK_INDCHECKS_NA}", "")
                .Replace("{NOTE_4}", txtNoteChecks.Text)
                .Replace("{CHK_REPAIRS_YES}", chkRepairs.Checked ? "checked" : "")
                .Replace("{CHK_REPAIRS_NO}", chkRepairs.Checked ? "" : "checked")
                .Replace("{CHK_REPAIRS_NA}", "")
                .Replace("{NOTE_5}", txtNoteRepairs.Text)
                .Replace("{ACA}", txtACA.Text);
            var strWorkCards = "";
            foreach (var row in dataGridView1.Rows)
            {
                var blah = (DataGridViewRow)row;
                strWorkCards += "<tr><td colspan=\"5\">" + blah.Cells[1].Value + " </td> <td colspan=\"3\">" + blah.Cells[2].Value + "</td></tr>";
            }
            content = content.Replace("{WORK_CARDS}", strWorkCards);
            var path = Path.Combine(currentDir, "temp.html");
            System.IO.File.WriteAllText(path, content);

            System.Diagnostics.Process.Start(path);
        }
    }
}
