From : https://www.thoughtco.com/use-sqlite-from-a-c-application-958255
{
	SQLite Manager : extension Firefox pour gérer manuellement...
	DLL : System.Data.SQLite.dll
	in code : 
		1. using System.Data.SQLite;
		2. const string filename = @"C:\cplus\tutorials\c#\SQLite\MyDatabase.sqlite";
		3. var conn = new SQLiteConnection("Data Source=" + filename + ";Version=3;");
		4. {
		    const string sql = "select * from friends;";
			sql += <other queries>
			try...
			conn.Open() ;
			DataSet ds = new DataSet() ;
			var da = new SQLiteDataAdapter(sql, conn) ;
			da.Fill(ds) ;
			catch...
		}
}

UUID in SQLite : 
INSERT INTO mytable (uuid)
VALUES (X'01020304050607080910111213141516');
